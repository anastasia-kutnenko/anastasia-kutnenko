//path to test report test-output/old/Default Suite/index.html
package homework17;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Random;

import static org.testng.Assert.assertEquals;
public class hmw17 {
    Random rand = new Random();

    @Test
    public void compareTwoRandomValues() {
        System.out.println("Test Case One with Thread Id - "
                + Thread.currentThread().getId());
        int firstNumber = rand.nextInt();
        int secondNumber = rand.nextInt();
        System.out.println(String.format("first number = %d, second number = %d", firstNumber, secondNumber));
        assertEquals(firstNumber, secondNumber);
    }

    @Test
    public void compareTwoStringValues() {
        System.out.println("Test Case Two with Thread Id - "
                + Thread.currentThread().getId());
        String value1 = "Test1";
        String value2 = "Test2";
        Assert.assertEquals(value1, value2);
    }

    @Test
    public void findElementInArray() {
        System.out.println("Test Case Three with Thread Id - "
                + Thread.currentThread().getId());
        int[] array = {1, 2, 3, 4, 5};
        final int element = 1;
        Assert.assertTrue(Arrays.stream(array).anyMatch(x -> x == element));
    }

    @Test
    public void compareEqualIntegerValues() {
        System.out.println("Test Case Four with Thread Id - "
                + Thread.currentThread().getId());
        int firstNumber = 4;
        int secondNumber = 4;
        assertEquals(firstNumber, secondNumber);
    }

    @Test
    public void compareEqualStringValues() {
        System.out.println("Test Case Five with Thread Id - "
                + Thread.currentThread().getId());
        String value1 = "Test1";
        String value2 = "Test1";
        Assert.assertEquals(value1, value2);
    }
}

