package main.java.homework13;

import java.util.Scanner;

public class Homework13 {
    public static void main(String args[]) {
        validation();
    }

    public static void validation() {
        Scanner scanner = new Scanner(System.in);
        boolean flag = false;
        while (flag == false) {
            System.out.println("Enter phone number: ");
            String phoneNumber = scanner.nextLine();
            if (phoneNumber.length() >= 9 & phoneNumber.length() <= 13) {
                if (phoneNumber.startsWith("+38063")
                        || phoneNumber.startsWith("+38098")
                        || phoneNumber.startsWith("+38096")
                        || phoneNumber.startsWith("+38067")
                        || phoneNumber.startsWith("+38093"))
                { System.out.println("Phone number is valid");
                flag = true; }
             else System.out.println("Phone number is invalid. Please try again once more");
            } else System.out.println("Phone number is invalid. Please try again once more");
        }
    }
}
