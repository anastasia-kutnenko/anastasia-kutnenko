package homework16;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import static homework16.Database.*;

public class App {

    /*
    Create a console application. Task manager.
    User should be able to create, edit, remove tasks.
    Data should be stored in app memory.

     */

    private TaskManagerIImpl taskManagerI = new TaskManagerIImpl();

    public static void main(String[] args) throws SQLException {
        App app = new App();
        menu();
        Scanner scanner = new Scanner(System.in);

        while (app.manageTask(scanner)) {
            menu();
        }
    }

    private static void menu() {
        System.out.println("\nHello" +
                "\nThis is the task manager supper application" +
                "\n1. Create task" +
                "\n2. Edit task" +
                "\n3. Remove task" +
                "\n4. Show all tasks" +
                "\n5. Exit  ");
    }

    private boolean manageTask(Scanner scanner) throws SQLException {
        int data = Integer.parseInt(scanner.next());
        Connection connection = connectToDatabase();
        Statement statement = createStatement(connection);
        switch (data) {
            case 1:
                Task task = taskManagerI.createTask(scanner);
                Database.createRecordInDataBase(statement, task);
                if (task == null) {
                    return false;
                }
                break;
            case 2:
                taskManagerI.editTask(statement, scanner);
                break;
            case 3:
                taskManagerI.removeTask(statement, scanner);
                break;
            case 4:
                taskManagerI.showAllTasks(statement);
                break;
            case 5:
                System.out.println("Closing application");
                closeConnection(statement, connection);

                System.exit(-1);
                break;
            default:
                System.out.println("Closing application");
                closeConnection(statement, connection);
                System.exit(-1);
        }
        return true;
    }
}
