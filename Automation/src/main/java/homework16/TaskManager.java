package homework16;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public interface TaskManager {
    Task createTask(Scanner scanner);

    boolean editTask(Statement statement, Scanner scanner) throws SQLException;

    boolean removeTask(Statement statement, Scanner scanner);

    void showAllTasks(Statement statement) throws SQLException;
}
