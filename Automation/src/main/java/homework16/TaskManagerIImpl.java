package homework16;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

public class TaskManagerIImpl implements TaskManager {

    @Override
    public Task createTask(Scanner scanner) {

        try {
            System.out.println("Enter task id");
            int id = Integer.parseInt(scanner.next());

            System.out.println("Enter title");
            String title = scanner.next();

            System.out.println("Enter description");
            String description = scanner.next();

            Date date = new Date();

            Task task = new Task(id, title, description, date);
            return task;

        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    @Override
    public boolean editTask(Statement statement, Scanner scanner) {
        try {
            System.out.println("Enter task id");
            int id = Integer.parseInt(scanner.next());

            if (Database.getTaskFromDatabaseById(statement, id) != null) {
                System.out.println("Enter title");
                String title = scanner.next();

                System.out.println("Enter description");
                String description = scanner.next();

                Date date = new Date();

                Task task = new Task(id, title, description, date);

                task.setTitle(title);
                task.setDescription(description);
                task.setDate(date);

                Database.updateRecordInDataBase(statement, task);

                return true;
            }
            return false;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    @Override
    public boolean removeTask(Statement statement, Scanner scanner) {
        try {
            System.out.println("Enter task id");
            int id = Integer.parseInt(scanner.next());

            Database.deleteRecordFromDatabaseById(statement, id);
            return true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    @Override
    public void showAllTasks(Statement statement) throws SQLException {
        List<Task> tasks = Database.getTasksFromDataBase(statement);
        for(Task task : tasks) {
            System.out.println(task);
        }
    }
}
