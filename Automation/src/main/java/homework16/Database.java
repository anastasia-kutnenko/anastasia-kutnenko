package homework16;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Database {
    public static Connection connectToDatabase() throws SQLException {

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("PostgreSQL JDBC Driver is not found. Include it in your library path ");
            e.printStackTrace();
        }

        Connection con = DriverManager.getConnection(
                "jdbc:postgresql://localhost:5432/taskmanager", "postgres",
                "postgres");

        return con;
    }

    public static Statement createStatement(Connection connection) throws SQLException {
        return connection.createStatement();
    }

    public static ResultSet returnResultSet(Statement stmt) throws SQLException {

        return stmt.executeQuery("SELECT * FROM tasks");
    }

    public static void createRecordInDataBase(Statement stmt, Task task) throws SQLException {
        String createStatement = "INSERT INTO tasks VALUES(" + task.getId() + " , '" + task.getTitle() + "', '"
                + task.getDescription() + "', '" + task.getDate() + "')";
        stmt.executeUpdate(createStatement);
    }

    public static void updateRecordInDataBase(Statement stmt, Task task) throws SQLException {
        String updateStatement = "UPDATE tasks SET title = '" + task.getTitle() + "', description = '"
                + task.getDescription() + "', created_date = '" + task.getDate() + "' WHERE id = " + task.getId() + "";
        stmt.executeUpdate(updateStatement);
    }

    public static Integer getTaskFromDatabaseById(Statement stmt, int taskId) throws SQLException {
        String getResultSetById = "SELECT * FROM tasks WHERE id = " + taskId + "";
        ResultSet resultSet = stmt.executeQuery(getResultSetById);
        Integer databaseId = null;
        while (resultSet.next()) {
            databaseId = resultSet.getInt("id");
        }
        return databaseId;
    }

    public static void deleteRecordFromDatabaseById(Statement stmt, int taskId) throws SQLException {
        String deleteRecordById = "DELETE FROM tasks WHERE id = " + taskId + "";
        stmt.executeUpdate(deleteRecordById);
    }

    public static List<Task> getTasksFromDataBase(Statement stmt) throws SQLException {
        ResultSet resultSet = returnResultSet(stmt);
        List<Task> tasks = new ArrayList<Task>();
        while (resultSet.next()) {

            int id = resultSet.getInt("id");
            String title = resultSet.getString("title");
            String description = resultSet.getString("description");
            Date createdDate = resultSet.getDate("created_date");
            Task task = new Task(id, title, description, createdDate);
            tasks.add(task);
        }
        return tasks;
    }

    public static void closeConnection(Statement statement, Connection connection) throws SQLException {
        statement.close();
        connection.close();
    }
}
