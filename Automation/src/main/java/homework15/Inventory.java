package homework15;

import java.util.Scanner;

public interface Inventory {
    boolean addProduct(Scanner scanner);
    void showInventoryValue(Scanner scanner);
    void showInventory(Scanner scanner);

}
