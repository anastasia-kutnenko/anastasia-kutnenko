package homework15;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class InventoryImpl implements Inventory{
    private Map<Integer, Product> database = new HashMap<>();

    public void inStock() {
        Product product1 = new Product("t-shirt", 35.5, 1);
        Product product2 = new Product("skirt", 40.9, 3);
        Product product3 = new Product("dress", 50.99, 5);
        database.put(1, product1);
        database.put(2, product2);
        database.put(3, product3);
    }

    @Override
    public boolean addProduct(Scanner scanner) {

        try {
            System.out.println("Enter product id");
            int id = Integer.parseInt(scanner.next());

            System.out.println("Enter name");
            String name = scanner.next();

            System.out.println("Enter price");
            double price = scanner.nextDouble();

            System.out.println("Enter quantity");
            int quantity = scanner.nextInt();

            Product product = new Product(name, price, quantity);
            database.putIfAbsent(id, product);

            return true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    @Override
    public void showInventoryValue(Scanner scanner) {
        double value = 0;
        for (Map.Entry entry : database.entrySet()) {
            value = value + database.get(entry.getKey()).getQuantity() * database.get(entry.getKey()).getPrice();
        }
        System.out.println(value);
    }

    @Override
    public void showInventory(Scanner scanner) {
        for (Map.Entry entry : database.entrySet()) {
            System.out.println(entry);
        }
    }
}
