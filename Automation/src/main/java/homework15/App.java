package homework15;

import java.util.Scanner;

public class App {
    static InventoryImpl inventory = new InventoryImpl();

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        App app = new App();
        inventory.inStock();
        menu();
        while (app.manageInventory(scanner)) {
            menu();
        }
    }

    public static void menu() {
        System.out.println("\nSelect one of the actions below: " +
                "\n1. Add product" +
                "\n2. Show inventory value" +
                "\n3. Show inventory" +
                "\n4. Exit");
    }

    public boolean manageInventory(Scanner scanner) {
        int data = Integer.parseInt(scanner.next());
        switch (data) {
            case 1:
                boolean result = inventory.addProduct(scanner);
                if (!result) {
                    return false;
                }
                break;
            case 2:
                inventory.showInventoryValue(scanner);
                break;
            case 3:
                inventory.showInventory(scanner);
                break;
            case 4:
                System.out.println("Closing application");
                System.exit(-1);
                break;
            default:
                System.out.println("Closing application");
                System.exit(-1);
        }
        return true;
    }
}
