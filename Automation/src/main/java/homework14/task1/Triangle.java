package homework14.task1;

public class Triangle extends Shape {
    double a;
    double b;
    double c;
    double perimeter;

    public Triangle(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    @Override
    public double calculateArea() {
        double halfPerimeter = calculatePerimeter() / 2;
        return Math.sqrt(halfPerimeter * (halfPerimeter - a) * (halfPerimeter - b) * (halfPerimeter - c));
    }

    @Override
    public double calculatePerimeter() {
        return perimeter = a + b + c;
    }
}
