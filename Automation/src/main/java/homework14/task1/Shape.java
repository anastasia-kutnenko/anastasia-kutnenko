package homework14.task1;

public abstract class Shape {
    public abstract double calculateArea() ;
    public abstract double calculatePerimeter();
}
