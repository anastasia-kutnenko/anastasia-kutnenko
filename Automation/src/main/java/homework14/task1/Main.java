package homework14.task1;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Select your figure: \n" +
                " - Rectangle\n" +
                " - Square\n" +
                " - Triangle\n" +
                " - Circle");
        String figureName = scanner.nextLine();
        if (figureName.equalsIgnoreCase(homework14.task1.Figures.RECTANGLE.toString())) {
            System.out.println("Input a: ");
            double a = scanner.nextDouble();
            System.out.println("Input b: ");
            double b = scanner.nextDouble();
           homework14.task1.Rectangle rectangle = new homework14.task1.Rectangle(a, b);
            System.out.println("Area of Rectangle = " + rectangle.calculateArea());
            System.out.println("Perimeter of Rectangle = " + rectangle.calculatePerimeter());
        } else if (figureName.equalsIgnoreCase(homework14.task1.Figures.SQUARE.toString())) {
            System.out.println("Input a: ");
            double a = scanner.nextDouble();
            homework14.task1.Square square = new homework14.task1.Square(a);
            System.out.println("Area of Square = " + square.calculateArea());
            System.out.println("Perimeter of Square = " + square.calculatePerimeter());
        } else if (figureName.equalsIgnoreCase(homework14.task1.Figures.TRIANGLE.toString())) {
            System.out.println("Input a: ");
            double a = scanner.nextDouble();
            System.out.println("Input b: ");
            double b = scanner.nextDouble();
            System.out.println("Input c: ");
            double c = scanner.nextDouble();
            homework14.task1.Triangle triangle = new homework14.task1.Triangle(a, b, c);
            System.out.println("Area of Triangle = " + triangle.calculateArea());
            System.out.println("Perimeter of Triangle = " + triangle.calculatePerimeter());
        } else if (figureName.equalsIgnoreCase(homework14.task1.Figures.CIRCLE.toString())) {
            System.out.println("Input radius of circle: ");
            double radius = scanner.nextDouble();
            homework14.task1.Circle circle = new homework14.task1.Circle(radius);
            System.out.println("Area of Circle = " + circle.calculateArea());
            System.out.println("Perimeter of Circle(circumference) = " + circle.calculatePerimeter());
        } else {
            System.out.println("Wrong figure. Please try one more time");
        }
    }
}
