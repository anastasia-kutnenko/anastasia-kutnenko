package homework14.task1;

public class Circle extends homework14.task1.Shape {
    double radius;
    final double pi = 3.1415;

    public Circle(double radius) {
        this.radius = radius;
    }

    @Override
    public double calculateArea() {
        return  pi * radius * radius;
    }

    @Override
    public double calculatePerimeter() {
        return 2 * pi * radius;
    }
}
