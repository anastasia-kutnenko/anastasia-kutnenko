package homework14.task2;

import java.util.Scanner;

public enum WashingTypes {
    DELICATE("DELICATE"),
    SYNTHETICS("SYNTHETICS"),
    COTTON("COTTON"),
    WOOL("WOOL");

    private String value;

    private WashingTypes(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }

    public static void isWashingTypeValid(Scanner scanner) {
        String input = scanner.next();
        boolean valid = false;
        try {
            for (WashingTypes type : WashingTypes.values()) {
                if (type.value.equalsIgnoreCase(input)) {
                    valid = true;
                }
            }
        } catch (IllegalArgumentException e) {
            System.out.println("Your washing type is invalid. Please try again");
            input = scanner.next();
        }
        if (valid == false) {
            System.out.println("Your washing type is invalid. Please try again");
            isWashingTypeValid(scanner);
        }
    }
}
