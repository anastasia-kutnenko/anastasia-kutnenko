package homework14.task2;

import java.util.Scanner;

public enum Temperature {
    COLD("30"),
    WARM("60"),
    HOT("90");
    public String value;

    private Temperature(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }

    public static void isTemperatureValid(Scanner scanner) {
        boolean valid = false;
        String input = scanner.next();
        try {
            for (Temperature type : Temperature.values()) {
                if (type.value.equalsIgnoreCase(input)) {
                    valid = true;
                }
            }
        } catch (IllegalArgumentException e) {
            System.out.println("Your temperature is invalid. Please try again");
            input = scanner.next();
        }
        if (valid == false) {
            System.out.println("Your temperature is invalid. Please try again");
            isTemperatureValid(scanner);
        }
    }
}