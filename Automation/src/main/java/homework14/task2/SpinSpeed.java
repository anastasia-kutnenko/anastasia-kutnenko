package homework14.task2;

import java.util.Scanner;

public enum  SpinSpeed {
    VERY_LOW("400"), LOW("600"), MEDIUM("800"), HIGH("900"), VERY_HIGH("1000");
    private String value;

    private SpinSpeed(String value) {
        this.value = value;
    }
    @Override public String toString() {
        return value;
    }

    public static void isSpinSpeedValid(Scanner scanner) {
        String input = scanner.next();
        boolean valid = false;
        try {
            for (SpinSpeed type : SpinSpeed.values()) {
                if (type.value.equalsIgnoreCase(input)) {
                    valid = true;
                }
            }
        } catch (IllegalArgumentException e) {
            input = scanner.next();
            System.out.println("Your spin speed is invalid. Please try again");
        }
        if (valid == false) {
                System.out.println("Your spin speed is invalid. Please try again");
                isSpinSpeedValid(scanner);
            }
    }
}
