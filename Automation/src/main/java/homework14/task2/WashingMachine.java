package homework14.task2;

import java.util.EnumSet;
import java.util.Scanner;

import static homework14.task2.SpinSpeed.isSpinSpeedValid;


public class WashingMachine {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        menu(scanner);
    }

    private static void washing(Scanner scanner) {
        for (homework14.task2.WashingTypes info : EnumSet.allOf(homework14.task2.WashingTypes.class)) {
            System.out.println(info);
        }
        homework14.task2.WashingTypes.isWashingTypeValid(scanner);

        System.out.println("Select spin speed");
        for (homework14.task2.SpinSpeed info : EnumSet.allOf(homework14.task2.SpinSpeed.class)) {
            System.out.println(info);
        }
        isSpinSpeedValid(scanner);


        System.out.println("Select temperature");
        for (homework14.task2.Temperature info : EnumSet.allOf(homework14.task2.Temperature.class)) {
            System.out.println(info);
        }
        homework14.task2.Temperature.isTemperatureValid(scanner);

        System.out.println("Set your washing time in seconds");
        homework14.task2.WashingTime.washingTimer(scanner);

        System.out.println("Washing is finished");
    }

    private static void menu(Scanner scanner) {
        System.out.println("1 - Select washing type \n2 - Exit");
        try {
            boolean exit = false;
            int data = scanner.nextInt();
            while (!exit) {
                switch (data) {
                    case 1:
                        washing(scanner);
                        menu(scanner);
                        break;
                    case 2:
                        System.out.println("Closing application");
                        System.exit(-1);
                        break;
                    default:
                        System.out.println("Your input is incorrect. Closing application");
                        System.exit(-1);
                }
            }
        } catch (Exception InputMismatchException) {
            System.out.println("Your input is incorrect. Try again");
            menu(scanner);
        }
    }
}