package homework14.task2;

import java.util.Scanner;

public class WashingTime {
    public static void washingTimer(Scanner scanner) {
        try {
            while (!scanner.hasNextInt()) {
                System.out.println("Your time is invalid. Please input correct time");
                String input = scanner.next();
            }
            int time = Integer.parseInt(scanner.next());
            Thread.sleep(time * 1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

