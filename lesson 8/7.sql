//SELECT films with Morgan Hopkins
SELECT film.film_id, film.title, actor.first_name, actor.last_name FROM film
JOIN film_actor on film.film_id = film_actor.film_id
JOIN actor on film_actor.actor_id = actor.actor_id
WHERE actor.actor_id = 113